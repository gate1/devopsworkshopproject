# Redux TodoMVC Example

This project template was built with [Create React App](https://github.com/facebookincubator/create-react-app), which provides a simple way to start React projects with no build configuration needed.

Projects built with Create-React-App include support for ES6 syntax, as well as several unofficial / not-yet-final forms of Javascript syntax such as Class Properties and JSX. See the list of [language features and polyfills supported by Create-React-App](https://github.com/facebookincubator/create-react-app/blob/master/packages/react-scripts/template/README.md#supported-language-features-and-polyfills) for more information.

## Available Scripts

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.<br>
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br>
You will also see any lint errors in the console.

### `npm run build`

Builds the app for production to the `build` folder.<br>
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.<br>
Your app is ready to be deployed!

### `npm run eject`

**Note: this is a one-way operation. Once you `eject`, you can’t go back!**

If you aren’t satisfied with the build tool and configuration choices, you can `eject` at any time. This command will remove the single build dependency from your project.

Instead, it will copy all the configuration files and the transitive dependencies (Webpack, Babel, ESLint, etc) right into your project so you have full control over them. All of the commands except `eject` will still work, but they will point to the copied scripts so you can tweak them. At this point you’re on your own.

You don’t have to ever use `eject`. The curated feature set is suitable for small and middle deployments, and you shouldn’t feel obligated to use this feature. However we understand that this tool wouldn’t be useful if you couldn’t customize it when you are ready for it.

<!-- gitlab ci validate installation -->
## npm i -g gitlab-ci-validate
## gitlab-ci-validate <yml file>


## git add .
## git commit -m "Add existing file"
## git push origin master

## kubectl apply -f nginx-deployment.yaml

kubectl port-forward redux-todomvc-5965b4fd6f-8qdrz 8080:80

## kubectl get deployments
## kubectl delete deployment todomvc-deployment

## kubectl get pods

## kubectl get services
## kubectl describe services redux-todomvc-service


## kubectl describe pod nginx-deployment-6b9bcd5bf5-2fqnq

<!-- minikube ip ile erisim icin -->
## <minikube ip> <nodePort>

<!-- gcloud ile deneme yapmaya baslamadan once -->
minikube stop
kubectl get pods




<!-- birden fazla kubernetes cluster da calisma ihtimalimiz var bunlarin birbirini ezmemesi icin current-context saglayacak -->
<!-- vim /Users/mustafaerbay/.kube/config
current-context: gke_sylvan-road-254811_us-central1-a_redux-todomvc -->
gcloud container clusters get-credentials redux-todomvc --zone us-central1-a --project sylvan-road-254811

<!-- gcloud auth login olmak icin -->
gcloud auth login


gcloud project list

gcloud config set project PROJECT_ID

gcloud container clusters
gcloud container clusters get-credentials redux-todomvc --zone us-central1-a --project sylvan-road-254811



<!-- google cloud asamalari
1 google cloud platform
2 IAM & Admin
3 project owner olarak service account olustur.
4 daha sonra json dosyasini indir ve asagida ki komutu calistir.

gcloud auth activate-service-account --key-file serviceaccount.json

5 serviceaccount u gitlab a encode ederek koymamiz gerekiyor

cat serviceaccount.json | base64  >  encoded_serviceaccount.json
 -->


 gcloud auth activate-service-account --key-file serviceaccount.json



gauge init js
gauge run specs

 graphana
 prometheus
 slack le bildirim atiliyor
 fleunt bit
 greylog
 openstack
 vagrant
 ansible > test kitchen

