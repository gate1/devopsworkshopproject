const {
  openBrowser,
  goto,
  write,
  press
//   click
} = require('taiko');
(async () => {
  try {
    await openBrowser();
    await goto("http://localhost:3000/");
    await write("taiko test automation");
    await press("Enter");
    // await click("Google'da Ara");
  } catch (error) {
    console.error(error);
  } finally {
    closeBrowser();
  }
})();
